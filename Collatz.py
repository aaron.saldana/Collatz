#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

from typing import IO, List
LENGTHS = [0] * 1000000

def collatz_read(string: str) -> List[int]:
    """
    read two ints
    string a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    line = string.split()
    return [int(line[0]), int(line[1])]

def collatz_print(write: IO[str], lower: int, upper: int, max_len: int) -> None:
    """
    print three ints
    write a writer
    lower the beginning of the range, inclusive
    upper the end       of the range, inclusive
    v the max cycle length
    """
    write.write(str(lower) + " " + str(upper) + " " + str(max_len) + "\n")

def evaluate(lower: int, upper: int):
    """Fills up the LENGTHS list
    with corresponding cycle lengths
    """
    for number in range(lower + 1, upper + 1):
        cycle_length = 0
        LENGTHS[0] = 1
        temp = number
        while temp > 1:
            if temp <= 1000000 and LENGTHS[temp - 1] != 0:
                cycle_length += LENGTHS[temp - 1]
                break
            if (temp % 2) == 0:
                temp = (temp // 2)
            else:
                temp = 3 * temp + 1
            cycle_length += 1
        if temp == 1:
            cycle_length += 1
        LENGTHS[number - 1] = cycle_length

def collatz_eval(lower: int, upper: int) -> int:
    """Determines the longest cycle
    length in a given range.
    """
    max = 1
    if lower <= upper:
        for number in range(lower, upper+1):
            if LENGTHS[number - 1] > max:
                max = LENGTHS[number-1]
    else:
        while lower > upper-1:
            if LENGTHS[lower - 1] > max:
                max = LENGTHS[lower-1]
            lower -= 1
    return max

def collatz_solve(read: IO[str], write: IO[str]) -> None:
    evaluate(1, 1000000)
    for string in read:
        lower, upper = collatz_read(string)
        max_length = collatz_eval(lower, upper)
        collatz_print(write, lower, upper, max_length)
