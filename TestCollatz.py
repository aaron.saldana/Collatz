#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# -------
# imports
# -------

from io       import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, evaluate

# -----------
# TestCollatz
# -----------

class TestCollatz (TestCase):
    evaluate(1, 1000000)
    # ----
    # read
    # ----

    def test_read(self):
        string    = "1 10\n"
        lower, upper = collatz_read(string)
        self.assertEqual(lower,  1)
        self.assertEqual(upper, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        value = collatz_eval(1, 10)
        self.assertEqual(value, 20)

    def test_eval_2(self):
        value = collatz_eval(100, 200)
        self.assertEqual(value, 125)

    def test_eval_3(self):
        value = collatz_eval(201, 210)
        self.assertEqual(value, 89)

    def test_eval_4(self):
        value = collatz_eval(900, 1000)
        self.assertEqual(value, 174)
        
    def test_eval_5(self):
        value = collatz_eval(100, 50)
        self.assertEqual(value, 119)
    
    def test_eval_6(self):
        value = collatz_eval(1, 1000000)
        self.assertEqual(value, 525)

    def test_eval_7(self):
        value = collatz_eval(1000000, 1)
        self.assertEqual(value, 525)

    def test_eval_8(self):
        value = collatz_eval(50, 50)
        self.assertEqual(value, 25)

    def test_eval_9(self):
        value = collatz_eval(853, 256)
        self.assertEqual(value, 171)

    def test_eval_10(self):
        value = collatz_eval(500000, 850000)
        self.assertEqual(value, 525)

    def test_eval_11(self):
        value = collatz_eval(999999, 1000000)
        self.assertEqual(value, 259)

    def test_eval_12(self):
        value = collatz_eval(1000000, 1000000)
        self.assertEqual(value, 153)

    def test_eval_13(self):
        value = collatz_eval(1, 1)
        self.assertEqual(value, 1)

    def test_eval_14(self):
        value = collatz_eval(789456, 123456)
        self.assertEqual(value, 509)

    def test_eval_15(self):
        value = collatz_eval(2, 524288)
        self.assertEqual(value, 470)

    # -----
    # print
    # -----

    def test_print(self):
        write = StringIO()
        collatz_print(write, 1, 10, 20)
        self.assertEqual(write.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        read = StringIO("1 10\n100 200\n201 210\n900 1000\n100 50\n1 1000000\n1000000 1\n50 50\n853 256\n500000 850000\n999999 1000000\n1000000 1000000\n1 1\n789456 123456\n2 524288\n")
        write = StringIO()
        collatz_solve(read, write)
        self.assertEqual(write.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n100 50 119\n1 1000000 525\n1000000 1 525\n50 50 25\n853 256 171\n500000 850000 525\n999999 1000000 259\n1000000 1000000 153\n1 1 1\n789456 123456 509\n2 524288 470\n")

# ----
# main
# ----

if __name__ == "__main__": #pragma: no cover
    main()
